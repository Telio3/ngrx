import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'superheros',
    loadChildren: () =>
      import('./superhero/superhero.module').then((m) => m.SuperheroModule),
    data: {preload: true}
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
