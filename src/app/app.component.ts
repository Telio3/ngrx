import {Component} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  template: `
    <mat-toolbar>
      <span>{{ 'HEADER.TITLE' | translate }}</span>
      <button class="button-menu" [routerLink]="['superheros']" mat-stroked-button>{{ "HEADER.MENU.SUPERHERO" | translate }}</button>
      <button class="button-menu" disabled mat-stroked-button>{{ "HEADER.MENU.SUPERHERO_TEAM" | translate }}</button>
      <span class="spacer"></span>
      <mat-form-field >
        <select matNativeControl #langSelect (change)="use(langSelect.value)">
          <option *ngFor="let lang of getLangs()"
                  [value]="lang"
                  [selected]="lang === currentLang()">
            {{ 'LANG.' + lang | uppercase | translate }}
          </option>
        </select>
      </mat-form-field>
    </mat-toolbar>
    <router-outlet></router-outlet>
  `,
  styles: [`
    .spacer{
      flex: 1 1 auto;
    }
    .button-menu {
      margin-left: 1rem;
    }
  `]
})
export class AppComponent {
  constructor(
    private translateService: TranslateService
  ) {

    translateService.addLangs(['en', 'fr', 'esp']);
    translateService.setDefaultLang('fr');

    const browserLang: string = translateService.getBrowserLang() ?? 'fr';
    translateService.use((browserLang.match(/en|fr|esp/) ? browserLang : 'fr'));
  }

  use(lang: string): Observable<string> {
    return this.translateService.use(lang)
  }

  currentLang(): string {
    return this.translateService.currentLang ?? this.translateService.defaultLang
  }

  getLangs(): string[] {
    return this.translateService.getLangs();
  }
}
