import { createAction, props } from '@ngrx/store';
import { Superhero } from 'src/app/superhero/models/superhero.model';

export const loadSuperheros = createAction(
  '[Superhero] Load Superheros'
);

export const loadSuperherosSuccess = createAction(
  '[Superhero] Load Superheros Success',
  props<{ data: Superhero[] }>()
);

export const loadSuperherosFailure = createAction(
  '[Superhero] Load Superheros Failure',
  props<{ error: any }>()
);

export const createSuperhero = createAction(
  '[Superhero] Create Superhero',
  props<{ data: Superhero }>()
);

export const createSuperheroSuccess = createAction(
  '[Superhero] Create Superhero Success',
  props<{ data: Superhero }>()
);

export const createSuperheroFailure = createAction(
  '[Superhero] Create Superhero Failure',
  props<{ error: any }>()
);

export const deleteSuperhero = createAction(
  '[Superhero] Delete Superhero',
  props<{ data: Superhero }>()
);

export const deleteSuperheroSuccess = createAction(
  '[Superhero] Delete Superhero Success',
  props<{ data: Superhero }>()
);

export const deleteSuperheroFailure = createAction(
  '[Superhero] Delete Superhero Failure',
  props<{ error: any }>()
);

export const selectSuperhero = createAction(
  '[Superhero] Select Superhero',
  props<{ id: string }>()
);

export const selectSuperheroSuccess = createAction(
  '[Superhero] Select Superhero Success',
  props<{ data: Superhero }>()
);

export const selectSuperheroFailure = createAction(
  '[Superhero] Select Superhero Failure',
  props<{ error: any }>()
);

export const updateSuperhero = createAction(
  '[Superhero] Update Superhero',
  props<{ data: Superhero }>()
);

export const updateSuperheroSuccess = createAction(
  '[Superhero] Update Superhero Success'
);

export const updateSuperheroFailure = createAction(
  '[Superhero] Update Superhero Failure',
  props<{ error: any }>()
);
