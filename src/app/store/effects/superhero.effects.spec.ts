import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { SuperheroEffects } from './superhero.effects';

describe('SuperheroEffects', () => {
  let actions$: Observable<any>;
  let effects: SuperheroEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        SuperheroEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.inject(SuperheroEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
