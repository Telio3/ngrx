import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap, catchError, of, map, tap } from 'rxjs';
import { Superhero } from 'src/app/superhero/models/superhero.model';
import { SuperherosService } from 'src/app/superhero/services/superheros.service';
import * as SuperheroActions from '../actions/superhero.actions';


@Injectable()
export class SuperheroEffects {
  public loadSuperheros$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SuperheroActions.loadSuperheros),
      switchMap(() => this.superherosService.list()),
      map((data: Superhero[]) => SuperheroActions.loadSuperherosSuccess({ data })),
      catchError((error) => of(SuperheroActions.loadSuperherosFailure({ error })))
    )
  );

  public createSuperhero$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SuperheroActions.createSuperhero),
      switchMap(({ data }) => this.superherosService.create(data)),
      map((data: Superhero) => SuperheroActions.createSuperheroSuccess({ data })),
      catchError((error) => of(SuperheroActions.createSuperheroFailure({ error })))
    )
  );

  public createSuperheroSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SuperheroActions.createSuperheroSuccess, SuperheroActions.updateSuperheroSuccess),
      tap(() => this.router.navigate(['/superheros'])),
    ),
    { dispatch: false }
  );

  public deleteSuperhero$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SuperheroActions.deleteSuperhero),
      switchMap(({ data }) => this.superherosService.delete(data).pipe(map(() => data))),
      map((data: Superhero) => SuperheroActions.deleteSuperheroSuccess({ data })),
      catchError((error) => of(SuperheroActions.deleteSuperheroFailure({ error })))
    )
  );

  public selectSuperhero$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SuperheroActions.selectSuperhero),
      switchMap(({ id }) => this.superherosService.getById(id)),
      map((data: Superhero) => SuperheroActions.selectSuperheroSuccess({ data })),
      catchError((error) => of(SuperheroActions.selectSuperheroFailure({ error })))
    )
  );

  public updateSuperhero$ = createEffect(() =>
    this.actions$.pipe(
      ofType(SuperheroActions.updateSuperhero),
      switchMap(({ data }) => this.superherosService.update(data)),
      map(() => SuperheroActions.updateSuperheroSuccess()),
      catchError((error) => of(SuperheroActions.updateSuperheroFailure({ error })))
    )
  );

  constructor(private actions$: Actions, private superherosService: SuperherosService, private router: Router) { }
}
