import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { hydrationMetaReducer } from './hydratation/hydratation.reducer';
import * as fromSuperhero from './reducers/superhero.reducer';

export interface AppState {
  [fromSuperhero.superheroFeatureKey]: fromSuperhero.SuperherosState;
};

export const reducers: ActionReducerMap<AppState> = {
  [fromSuperhero.superheroFeatureKey]: fromSuperhero.superHerosReducer,
};

export const metaReducers: MetaReducer<AppState>[] = [
  hydrationMetaReducer
];
