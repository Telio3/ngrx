import { createReducer, on } from '@ngrx/store';
import { Superhero } from 'src/app/superhero/models/superhero.model';
import * as SuperheroActions from '../actions/superhero.actions';

export const superheroFeatureKey = 'superhero';

export interface SuperherosState {
  superheros: Superhero[];
  selectedSuperHero: Superhero | null;
  loaded: boolean;
  loading: boolean;
}

export const initialState: SuperherosState = {
  superheros: [],
  selectedSuperHero: null,
  loaded: false,
  loading: false
};

export const superHerosReducer = createReducer(
  initialState,

  on(SuperheroActions.loadSuperheros, (state) => ({ ...state, loading: true })),
  on(SuperheroActions.loadSuperherosSuccess, (state, { data }) => ({ ...state, superheros: data, loading: false, loaded: true })),
  on(SuperheroActions.loadSuperherosFailure, (state, { error }) => {
    console.error(error);
    return { ...state, loading: false, loaded: false };
  }),

  on(SuperheroActions.createSuperhero, (state) => ({ ...state, loading: true })),
  on(SuperheroActions.createSuperheroSuccess, (state, { data }) => ({ ...state, superheros: [...state.superheros, data], loading: false, loaded: true })),
  on(SuperheroActions.createSuperheroFailure, (state, { error }) => {
    console.error(error);
    return { ...state, loading: false, loaded: false };
  }),

  on(SuperheroActions.deleteSuperhero, (state) => ({ ...state, loading: true })),
  on(SuperheroActions.deleteSuperheroSuccess, (state, { data }) => ({ ...state, superheros: state.superheros.filter((superhero) => superhero.id !== data.id), loading: false, loaded: true })),
  on(SuperheroActions.deleteSuperheroFailure, (state, { error }) => {
    console.error(error);
    return { ...state, loading: false, loaded: false };
  }),

  on(SuperheroActions.selectSuperhero, (state) => ({ ...state, selectedSuperHero: null, loading: true })),
  on(SuperheroActions.selectSuperheroSuccess, (state, { data }) => ({ ...state, selectedSuperHero: data, loading: false, loaded: true })),
  on(SuperheroActions.selectSuperheroFailure, (state, { error }) => {
    console.error(error);
    return { ...state, loading: false, loaded: false };
  }),

  on(SuperheroActions.updateSuperhero, (state) => ({ ...state, loading: true })),
  on(SuperheroActions.updateSuperheroSuccess, (state) => ({ ...state, selectedSuperHero: null, loading: false, loaded: true })),
  on(SuperheroActions.updateSuperheroFailure, (state, { error }) => {
    console.error(error);
    return { ...state, loading: false, loaded: false };
  }),
);
