import { createSelector } from '@ngrx/store';
import { AppState } from '../index';
import * as fromSuperhero from '../reducers/superhero.reducer';

export const selectFeature = (state: AppState) => state[fromSuperhero.superheroFeatureKey];

export const selectSuperheroList = createSelector(
    selectFeature,
    (state: fromSuperhero.SuperherosState) => state.superheros
);

export const selectSuperhero = createSelector(
    selectFeature,
    (state: fromSuperhero.SuperherosState) => state.selectedSuperHero
);
