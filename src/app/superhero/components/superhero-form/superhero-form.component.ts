import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { SuperheroEditorEnum } from "../../models/superhero-editor.enum";
import { Superhero } from "../../models/superhero.model";

@Component({
  selector: 'app-superhero-form',
  templateUrl: './superhero-form.component.html',
  styles: [`
    form {
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      margin: 2rem;

      mat-form-field {
        width: 100%;
      }
    }
    .form-action {
      display: flex;
      justify-content: space-between;
      width: 100%
    }
  `]
})
export class SuperheroFormComponent implements OnInit {
  public form!: FormGroup;
  @Input() initData: Superhero = { name: '', editor: undefined };
  @Output() submitted: EventEmitter<Superhero> = new EventEmitter<Superhero>();
  public superheroEditor = Object.values(SuperheroEditorEnum);

  constructor(private fb: FormBuilder) { }

  public ngOnInit(): void {
    this._createForm();
  }

  private _createForm(): void {
    this.form = this.fb.group({
      name: [this.initData.name, Validators.required],
      editor: [this.initData.editor],
    }
    );
  }

  public onSubmit(): void {
    this.submitted.emit(this.form.value);
  }

  public displayError(control: string, error: string): boolean {
    return (this.form.controls[control].errors !== null && this.form.controls[control].errors!![error]) && this.form.controls[control].touched;
  }
}
