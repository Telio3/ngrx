import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {map, startWith, switchMap} from "rxjs/operators";
import {Title} from "@angular/platform-browser";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-superhero-layout',
  template: `
    <mat-toolbar color="accent">
      <span>{{ pageTitle }}</span>
      <span class="spacer"></span>
      <button color="primary" [routerLink]="['add']" mat-raised-button>{{ "SUPERHERO.MENU.BUTTONS.ADD" | translate }}</button>
    </mat-toolbar>
    <div>
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [`
    .spacer{
      flex: 1 1 auto;
    }
  `]
})
export class SuperheroLayoutComponent implements OnInit {
  pageTitle!: string;

  constructor(
    private title: Title,
    private route: ActivatedRoute,
    private translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.translateService.onLangChange.pipe(
      startWith(this.translateService.currentLang)
    ).subscribe(() =>
      this.route.firstChild?.data
        .pipe(
          map(data => data['pageTitle']),
          switchMap((key => this.translateService.get(`SUPERHERO.TITLE.${key}`)))
        )
        .subscribe(title => {
          this.pageTitle = title
          this.title.setTitle(title)
        })
    )
  }
}
