export enum SuperheroEditorEnum {
  DC_COMICS = 'DC Comics',
  MARVEL = 'Marvel',
  DARK_HORSE = 'Dark Horse',
  IMAGE = 'Image'
}

