import { SuperheroEditorEnum } from "./superhero-editor.enum";

export interface Superhero {
  id?: string;
  name: string;
  editor?: SuperheroEditorEnum;
}
