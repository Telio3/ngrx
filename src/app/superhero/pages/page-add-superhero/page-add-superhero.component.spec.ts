import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageAddSuperheroComponent } from './page-add-superhero.component';

describe('PageAddSuperheroComponent', () => {
  let component: PageAddSuperheroComponent;
  let fixture: ComponentFixture<PageAddSuperheroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageAddSuperheroComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PageAddSuperheroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
