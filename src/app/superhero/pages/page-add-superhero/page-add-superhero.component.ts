import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { Superhero } from '../../models/superhero.model';
import * as SuperheroActions from '../../../store/actions/superhero.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-add-superhero',
  template: `
    <app-superhero-form (submitted)="addSuperhero($event)"></app-superhero-form>
  `,
  styles: [
  ]
})
export class PageAddSuperheroComponent {
  constructor(private store: Store<AppState>) { }

  public addSuperhero(superhero: Superhero): void {
    this.store.dispatch(SuperheroActions.createSuperhero({ data: superhero }));
  }
}
