import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageListSuperherosComponent } from './page-list-superheros.component';

describe('PageListSuperherosComponent', () => {
  let component: PageListSuperherosComponent;
  let fixture: ComponentFixture<PageListSuperherosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageListSuperherosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PageListSuperherosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
