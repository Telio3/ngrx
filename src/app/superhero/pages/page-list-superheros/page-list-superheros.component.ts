import { Component, OnInit } from '@angular/core';
import { Superhero } from "../../models/superhero.model";
import { BehaviorSubject, map, Observable, tap } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { AppState } from 'src/app/store';
import * as SuperheroActions from '../../../store/actions/superhero.actions';
import { selectSuperheroList } from 'src/app/store/selectors/superhero.selectors';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-list-superheros',
  template: `
    <table mat-table [dataSource]="dataSource">
      <!-- Name Column -->
      <ng-container matColumnDef="name">
        <th mat-header-cell *matHeaderCellDef> {{ 'SUPERHERO.LIST.TABLE.HEADER.NAME' | translate }} </th>
        <td mat-cell *matCellDef="let element"> {{element.name}} </td>
      </ng-container>

      <!-- Editor Column -->
      <ng-container matColumnDef="editor">
        <th mat-header-cell *matHeaderCellDef> {{ 'SUPERHERO.LIST.TABLE.HEADER.EDITOR' | translate }} </th>
        <td mat-cell *matCellDef="let element"> {{element.editor}} </td>
      </ng-container>

      <ng-container matColumnDef="edit">
        <th mat-header-cell *matHeaderCellDef></th>
        <td mat-cell *matCellDef="let element">
          <button mat-raised-button color="primary" (click)="updateSuperhero(element)">
            {{ 'SUPERHERO.LIST.TABLE.BUTTON.EDIT' | translate }}
          </button>
        </td>
      </ng-container>

      <ng-container matColumnDef="delete">
      <th mat-header-cell *matHeaderCellDef></th>
        <td mat-cell *matCellDef="let element">
          <button mat-raised-button color="primary" (click)="deleteSuperhero(element)">
          {{ 'SUPERHERO.LIST.TABLE.BUTTON.DELETE' | translate }}
          </button>
        </td>
      </ng-container>

      <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
      <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>
    </table>
  `,
  styles: [`
    table {
      width: 100%;
    }
  `]
})
export class PageListSuperherosComponent implements OnInit {

  public displayedColumns: string[] = ['name', 'editor', 'edit', 'delete'];
  public dataSource: Superhero[] = [];
  public superheroState$: Observable<Superhero[]>;
  public superheroNames$ = new BehaviorSubject<string[]>([]);

  constructor(private store: Store<AppState>, private router: Router) {
    this.superheroState$ = store.pipe(select(selectSuperheroList));
  }

  public ngOnInit(): void {
    this.store.dispatch(SuperheroActions.loadSuperheros());

    this.superheroState$
      .pipe(
        tap((superheros: Superhero[]) => this.superheroNames$.next(superheros.map((superhero: Superhero) => superhero.name))),
        map((superheros: Superhero[]) => superheros.map((superhero: Superhero) => ({ ...superhero, name: superhero.name.toUpperCase() })))
      )
      .subscribe((superheros: Superhero[]) => {
        this.dataSource = superheros;
      })
  }

  public deleteSuperhero(superhero: Superhero): void {
    this.store.dispatch(SuperheroActions.deleteSuperhero({ data: superhero }));
  }

  public updateSuperhero(superhero: Superhero): void {
    this.router.navigate(['superheros', superhero.id]);
  }
}
