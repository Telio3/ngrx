import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageUpdateSuperheroComponent } from './page-update-superhero.component';

describe('PageUpdateSuperheroComponent', () => {
  let component: PageUpdateSuperheroComponent;
  let fixture: ComponentFixture<PageUpdateSuperheroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageUpdateSuperheroComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PageUpdateSuperheroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
