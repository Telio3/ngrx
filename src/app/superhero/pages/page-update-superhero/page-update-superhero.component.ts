import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/store';
import { selectSuperhero } from 'src/app/store/selectors/superhero.selectors';
import * as SuperheroActions from '../../../store/actions/superhero.actions';
import { Superhero } from '../../models/superhero.model';

@Component({
  selector: 'app-page-update-superhero',
  template: `<app-superhero-form *ngIf="superheroState$ | async as superheroState" [initData]="superheroState" (submitted)="updateSuperhero($event)"></app-superhero-form>`
})
export class PageUpdateSuperheroComponent implements OnInit {

  private _id = this.route.snapshot.paramMap.get('id');
  public superheroState$: Observable<Superhero | null>;

  constructor(private route: ActivatedRoute, private store: Store<AppState>) {
    this.superheroState$ = store.pipe(select(selectSuperhero));
  }

  public ngOnInit(): void {
    if (this._id) this.store.dispatch(SuperheroActions.selectSuperhero({ id: this._id }));
  }

  public updateSuperhero(superhero: Superhero): void {
    if (this._id) {
      const s = { id: this._id, ...superhero };
      this.store.dispatch(SuperheroActions.updateSuperhero({ data: s }));
    }
  }
}
