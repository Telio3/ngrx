import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Superhero } from '../models/superhero.model';
import { v4 as uuidv4 } from 'uuid';

@Injectable({
  providedIn: 'root'
})
export class SuperherosService {

  private urlApi = environment.urlApi;

  constructor(private http: HttpClient) { }

  public list(): Observable<Superhero[]> {
    return this.http.get<Superhero[]>(`${this.urlApi}/superheros`);
  }

  public getById(id: string): Observable<Superhero> {
    return this.http.get<Superhero>(`${this.urlApi}/superheros/${id}`);
  }

  public create(superhero: Superhero): Observable<Superhero> {
    const s = { id: uuidv4(), ...superhero };
    return this.http.post<Superhero>(`${this.urlApi}/superheros`, s);
  }

  public update(superhero: Superhero): Observable<Superhero> {
    return this.http.put<Superhero>(`${this.urlApi}/superheros/${superhero.id}`, superhero);
  }

  public delete(superhero: Superhero): Observable<void> {
    return this.http.delete<void>(`${this.urlApi}/superheros/${superhero.id}`);
  }
}
