import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageListSuperherosComponent } from "./pages/page-list-superheros/page-list-superheros.component";
import { PageAddSuperheroComponent } from "./pages/page-add-superhero/page-add-superhero.component";
import { SuperheroLayoutComponent } from "./layouts/superhero-layout/superhero-layout.component";
import { PageUpdateSuperheroComponent } from './pages/page-update-superhero/page-update-superhero.component';

const routes: Routes = [{
  path: '', component: SuperheroLayoutComponent, children: [
    { path: '', component: PageListSuperherosComponent, data: { pageTitle: 'LIST' } },
    { path: 'add', component: PageAddSuperheroComponent, data: { pageTitle: 'ADD' } },
    { path: ':id', component: PageUpdateSuperheroComponent, data: { pageTitle: 'EDIT' } }
  ]
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SuperheroRoutingModule { }
