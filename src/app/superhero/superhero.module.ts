import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuperheroFormComponent } from './components/superhero-form/superhero-form.component';
import {SuperheroRoutingModule} from "./superhero-routing.module";
import { PageAddSuperheroComponent } from './pages/page-add-superhero/page-add-superhero.component';
import { PageListSuperherosComponent } from './pages/page-list-superheros/page-list-superheros.component';
import {SuperheroLayoutComponent} from "./layouts/superhero-layout/superhero-layout.component";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatTableModule} from "@angular/material/table";
import {TranslateModule} from "@ngx-translate/core";
import {MatButtonModule} from "@angular/material/button";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {MatInputModule} from "@angular/material/input";
import { PageUpdateSuperheroComponent } from './pages/page-update-superhero/page-update-superhero.component';



@NgModule({
  declarations: [
    SuperheroLayoutComponent,
    SuperheroFormComponent,
    PageAddSuperheroComponent,
    PageListSuperherosComponent,
    PageUpdateSuperheroComponent
  ],
  imports: [
    CommonModule,
    SuperheroRoutingModule,
    MatToolbarModule,
    MatTableModule,
    TranslateModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule
  ]
})
export class SuperheroModule { }
